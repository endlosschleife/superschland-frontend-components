'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.CompetitionModeService
 * @description
 * # CompetitionService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('Superschland.CompetitionModeService', [function () {

		var _modes = [
			"Tournament",
			"League",
		];

		this.getModes = function(callbackFunction) {
			callbackFunction(_modes);
		};
	}
]);
