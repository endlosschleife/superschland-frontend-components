'use strict';

/**
 * @ngdoc service
 * @name superschlandService.ResetService
 * @description
 * # ResetService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
  .service('Superschland.ResetService', function ($http, BACKEND_URL) {

    this.reset = function(competitionId, deleteStatistics, deleteTeams, deleteMatches, deleteGroups, callbackFunction) {
        $http({
            data: {
                deleteTeams: deleteTeams,
                deleteMatches: deleteMatches,
                deleteStatistics: deleteStatistics,
                deleteGroups: deleteGroups
            },
            url: BACKEND_URL + '/reset/competition/' + competitionId,
            method: 'DELETE',
            headers: {"Content-Type": "application/json;charset=utf-8"}
        }).then(function(response) {
            callbackFunction();
        }, function(response) {

        });
    };

  });
