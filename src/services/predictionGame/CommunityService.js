'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.SuperschlandCommunityService
 * @description
 * # SuperschlandCommunityService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('SuperschlandCommunityService', ['$http', 'BACKEND_URL', '$q', function ($http, BACKEND_URL, $q) {

		this.getCommunities = function(communityScope) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/community', {
				params: {
					'scope': communityScope
				}
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		this.getCommunityByShortcut = function(shortcut) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/community/' + shortcut).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		this.add = function(shortcut, name, description, password, competitionId) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/community', {
				"shortcut": shortcut,
				"name": name,
				"description": description,
				"password": password,
				"competitionId": competitionId
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.join = function(shortcut, password) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/community/' + shortcut + "/user", {
				"password": password
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.leave = function(communityId) {
			var deferred = $q.defer();
			$http({
				url: BACKEND_URL + '/community/' + communityId + "/user",
				method: 'DELETE',
				headers: {"Content-Type": "application/json;charset=utf-8"}
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function(response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

	}
	]);
