'use strict';

/**
 * @ngdoc service
 * @name superschlandService.CompetitionPluginImportService
 * @description
 * # CompetitionPluginImportService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
	.service('SuperschlandCompetitionPluginImportService', function ($http, BACKEND_URL, $q) {

		this.importGroups = function (competitionId) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/competition/' + competitionId + '/import/groups')
				.then(function (response) {
					deferred.resolve(response.data);
				}, function (response) {
					deferred.reject(response.data);
				});
			return deferred.promise;
		};

		this.importMatches = function (competitionId) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/competition/' + competitionId + '/import/matches')
				.then(function (response) {
					deferred.resolve(response.data);
				}, function (response) {
					deferred.reject(response.data);
				});
			return deferred.promise;
		};


	});
