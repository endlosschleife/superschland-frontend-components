'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.TeamService
 * @description
 * # TeamService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('Superschland.TeamService', ['$http', '$q', 'BACKEND_URL', function ($http, $q, BACKEND_URL) {

		this.getTeams = function(callbackFunction) {

			$http.get(BACKEND_URL + '/teams').then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.add = function(name, shortName, teamType, callbackFunction, errorCallback) {

			$http.post(BACKEND_URL + '/team', {
				'name': name,
				'shortName': shortName,
				'teamType': teamType,
			})
			.then(function(response) {
				callbackFunction(response.data);
			})
			.catch(function(response) {
				errorCallback(response);
			});

		};

		this.getTeamsByTeamType = function(teamType, callbackFunction) {

			$http.get(BACKEND_URL + '/teams', {
				params: {
					'teamType': teamType
				}
			}).then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.getTeamsByCompetition = function(competitionId, callbackFunction) {

			$http.get(BACKEND_URL + '/competition/' + competitionId + '/teams').then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.addTeamByCompetition = function(competitionId, teamId, callbackFunction) {

			$http.post(BACKEND_URL + '/competition/' + competitionId + '/team/' + teamId)
			.then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.removeTeamByCompetition = function(competitionId, teamId, callbackFunction) {

			$http({
				url: BACKEND_URL + '/competition/' + competitionId + '/team/' + teamId,
				method: 'DELETE',
				headers: {"Content-Type": "application/json;charset=utf-8"}
			}).then(function(response) {
				callbackFunction(response.data);
			}, function(response) {
				callbackFunction(response.data);
			});

		};

		this.setIcon = function(teamId, fileData) {

			var fd = new FormData();
			fd.append('file', fileData);

			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/team/' + teamId + '/icon', fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				})
				.then(function success(response) {
					deferred.resolve(response.data);
				}, function error(response) {
					deferred.reject(response.data);
				});
			return deferred.promise;
		};

		this.deleteIcon = function(teamId) {

			var deferred = $q.defer();
			$http({
				url: BACKEND_URL + '/team/' + teamId + '/icon',
				method: 'DELETE',
				headers: {"Content-Type": "application/json;charset=utf-8"}
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function(response) {
				deferred.reject(response.data);
			});

			return deferred.promise;
		};

	}
]);
