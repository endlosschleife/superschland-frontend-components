'use strict';

/**
 * @ngdoc service
 * @name superschlandService.MatchService
 * @description
 * # MatchService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
    .service('Superschland.MatchService', function ($http, $q, BACKEND_URL) {

        this.getMatchesByGroup = function (groupId, successCallback) {

            $http.get(BACKEND_URL + '/group/' + groupId + '/matches').then(function (response) {
                successCallback(response.data);
            }, function (response) {
                console.error(response);
            });

        };

        this.getMatchesByCompetition = function (competitionId, successCallback) {

            $http.get(BACKEND_URL + '/competition/' + competitionId + '/matches').then(function (response) {
                successCallback(response.data);
            }, function (response) {
                console.error(response);
            });

        };

        this.add = function (groupId, homeTeamId, guestTeamId, kickoff, successCallback, errorCallback) {
            var matchRequest = {
                'groupId': groupId,
                'homeTeamId': homeTeamId,
                'guestTeamId': guestTeamId,
                'kickoff': kickoff
            };
            $http.post(BACKEND_URL + '/match', matchRequest)
                .then(function (response) {
                    successCallback(response.data);
                })
                .catch(function (response) {
                    errorCallback(response);
                });
        };

        this.remove = function (matchId, callbackFunction) {

            $http({
                url: BACKEND_URL + '/match/' + matchId,
                method: 'DELETE',
                headers: {"Content-Type": "application/json;charset=utf-8"}
            }).then(function (response) {
                callbackFunction();
            }, function (response) {
                callbackFunction();
            });

        };

        this.update = function (matchId, groupId, homeTeamId, guestTeamdId, kickoff) {

            var matchObj = {
                "groupId": groupId,
                "guestTeamId": guestTeamdId,
                "homeTeamId": homeTeamId,
                "kickoff": kickoff,
            };

            var deferred = $q.defer();
            $http.put(BACKEND_URL + '/match/' + matchId, matchObj)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        this.updateResult = function (matchId, goalsHome, goalsGuest, isFinished, successCallback, errorCallback) {

            var resultObj = {
                "goalsHome": goalsHome,
                "goalsGuest": goalsGuest,
                "finished": isFinished,
            };

            $http.put(BACKEND_URL + '/match/' + matchId + '/result', resultObj)
                .then(function (response) {
                    successCallback(response.data);
                })
                .catch(function (response) {
                    errorCallback(response);
                });
        };

        this.updateResultByPlugin = function (matchId) {
            var deferred = $q.defer();
            $http.post(BACKEND_URL + '/match/' + matchId + '/result', {})
                .then(function success(response) {
                    deferred.resolve(response.data);
                }, function error(response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        this.deleteResult = function (matchId, successCallback, errorCallback) {
            $http({
                url: BACKEND_URL + '/match/' + matchId + '/result',
                method: 'DELETE',
                headers: {"Content-Type": "application/json;charset=utf-8"}
            }).then(function (response) {
                successCallback(response.data);
            }, function (response) {
                errorCallback();
            });
        };

    });
