'use strict';

/**
 * @ngdoc service
 * @name superschlandService.GroupService
 * @description
 * # GroupService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
  .service('Superschland.GroupService', function ($http, $q, BACKEND_URL) {

    this.update = function(group, successCallback) {
        $http.put(BACKEND_URL + '/group/' + group.id, group).then(function(response) {
            successCallback(response.data);
        }, function (response) {
            console.error(response);
        });
    };

    this.getMatches = function(groupId) {
        // TODO:
    };

    this.getStatistic = function(groupId) {
        // TODO:
    };

    this.getGroupsByCompetition = function(competitionId, callbackFunction) {

      $http.get(BACKEND_URL + '/competition/' + competitionId + '/groups').then(function(response) {
          callbackFunction(response.data);
      }, function (response) {
          callbackFunction(response.data);
      });

    };

    this.removeGroupByCompetition = function(competitionId, group, callbackFunction) {
      $http({
          url: BACKEND_URL + '/competition/' + competitionId + '/group',
          method: 'DELETE',
          data: group,
          headers: {"Content-Type": "application/json;charset=utf-8"}
      }).then(function(response) {
          callbackFunction();
      }, function(response) {
          callbackFunction();
      });

    };

    this.addGroupByCompetition = function(competitionId, groupName, callbackFunction) {

      $http.post(BACKEND_URL + '/competition/' + competitionId + '/group', {
          name: groupName
      }).then(function(response) {
          callbackFunction(response.data);
      }, function (response) {
          callbackFunction(response.data);
      });

    };

    this.getMatchesByGroup = function(competitionId, groupId) {
        var deferred = $q.defer();
        $http.get(BACKEND_URL + '/competition/' + competitionId + '/group', {
            params: {
                groupId: groupId
            }
        }).then(function success(response) {
            deferred.resolve(response.data);
        }, function error(response) {
            deferred.reject(response.data);
        });
        return deferred.promise;
    };

  });
