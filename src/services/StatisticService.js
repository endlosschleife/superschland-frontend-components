'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.StatisticService
 * @description
 * # StatisticService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
  .service('Superschland.StatisticService', function ($http, BACKEND_URL) {

    this.getStatistic = function(statisticId, successCallback) {
      $http.get(BACKEND_URL + '/statistic/' + statisticId).then(function(response) {
        successCallback(response.data);
      }, function (response) {
        console.error(response);
      });
    };

	this.update = function(statisticId, successCallback) {
	  $http.put(BACKEND_URL + '/statistic/' + statisticId, {}).then(function(response) {
		  successCallback(response.data);
	  }, function (response) {
		  console.error(response);
	  });
	};

	this.getStatistics = function(competitionId, successCallback, errorCallback) {
	  $http.get(BACKEND_URL + '/competition/' + competitionId + '/statistics').then(function(response) {
	      successCallback(response.data);
	  }, function (response) {
		  errorCallback();
	  });
	};

    this.add = function(competitionId, name, successCallback, errorCallback) {
      $http.post(BACKEND_URL + '/statistic', {
	      'competitionId': competitionId,
	      'name': name,
      })
      .then(function(response) {
          successCallback(response.data);
      })
      .catch(function(response) {
          errorCallback(response);
      });
    };

    this.remove = function(statisticId, successCallback, errorCallback) {

      $http({
          url: BACKEND_URL + '/statistic/' + statisticId,
          method: 'DELETE',
          headers: {"Content-Type": "application/json;charset=utf-8"}
      }).then(function(response) {
          successCallback();
      }, function(response) {
          errorCallback();
      });

    };

	this.addRow = function(statisticId, teamId, successCallback, errorCallback) {
	  $http.post(BACKEND_URL + '/statistic/' + statisticId + '/row', {
			  'teamId': teamId
		  })
		  .then(function(response) {
			  successCallback(response.data);
		  })
		  .catch(function(response) {
			  errorCallback(response);
		  });
	};

	this.removeRow = function(statisticId, statisticRowId, successCallback, errorCallback) {
	  $http({
		  url: BACKEND_URL + '/statistic/' + statisticId + '/row/' + statisticRowId,
		  method: 'DELETE',
		  headers: {"Content-Type": "application/json;charset=utf-8"}
	  }).then(function(response) {
		  successCallback();
	  }, function(response) {
		  errorCallback();
	  });
	};

  });
