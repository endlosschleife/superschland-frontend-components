'use strict';

/**
 * @ngdoc service
 * @name superschlandService.CompetitionPluginSettingsService
 * @description
 * # CompetitionPluginSettingsService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
	.service('Superschland.CompetitionPluginSettingsService', function ($http, BACKEND_URL) {

		this.update = function (competitionId, pluginName, configuration, autoMatchResultEnabled,
		                        autoMatchKickoffEnabled, callbackFunction) {
			$http.put(BACKEND_URL + '/competition/' + competitionId + '/plugin-settings', {
					'pluginName': pluginName,
					'configuration': configuration,
					'autoMatchKickoffEnabled': autoMatchKickoffEnabled,
					'autoMatchResultEnabled': autoMatchResultEnabled
				})
				.then(function (response) {
					callbackFunction(response.data);
				}, function (response) {

				});
		};

		this.delete = function (competitionId, callbackFunction) {
			$http.delete(BACKEND_URL + '/competition/' + competitionId + '/plugin-settings')
				.then(function (response) {
					callbackFunction(response.data);
				}, function (response) {

				});
		};

		this.getSettings = function (competitionId, callbackFunction) {
			$http.get(BACKEND_URL + '/competition/' + competitionId + '/plugin-settings')
				.then(function (response) {
					callbackFunction(response.data);
				}, function (response) {

				});
		};

	});
