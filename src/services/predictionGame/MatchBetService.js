'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.SuperschlandCommunityService
 * @description
 * # SuperschlandCommunityService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('SuperschlandMatchBetService', ['$http', 'BACKEND_URL', '$q', function ($http, BACKEND_URL, $q) {

		this.getBets = function(predictionGameId, page) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/prediction-game/' + predictionGameId + '/bet', {
				params: {
					'page': page
				}
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};


		this.save = function(predictionGameId, matchId, goalsHome, goalsGuest, isJoker) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/prediction-game/' + predictionGameId + '/match/' + matchId + '/bet', {
				"goalsHome": goalsHome,
				"goalsGuest": goalsGuest,
				"joker": isJoker,
			}).then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.getJokerOverview = function(predictionGameId) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/prediction-game/' + predictionGameId + '/joker-overview')
			.then(function(response) {
				deferred.resolve(response.data);
			}, function (response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

	}
	]);
