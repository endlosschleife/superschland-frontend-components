'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.UserService
 * @description
 * # UserService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('Superschland.UserService', function ($http, BACKEND_URL, $q) {

		this.getUser = function() {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/user').then(function success(response) {
				deferred.resolve(response.data);
			}, function error(response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.add = function (username, password, email) {
			var deferred = $q.defer();
			$http.post(BACKEND_URL + '/user', {
				'username' : username,
				'password' : password,
				'email' : email
			}).then(function success(response) {
				deferred.resolve(response.data);
			}, function error(response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.setPassword = function(oldPassword, newPassword, newPasswordRepeat) {
			var deferred = $q.defer();
			$http.put(BACKEND_URL + '/user/password', {}, {
				params: {
					'oldPassword' : oldPassword,
					'newPassword' : newPassword,
					'newPasswordRepeat': newPasswordRepeat,
				},
			}).then(function success(response) {
				deferred.resolve(response.data);
			}, function error(response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

		this.setEmail = function(email) {
			var deferred = $q.defer();
			$http.put(BACKEND_URL + '/user/email', {}, {
				params: {
					'email' : email
				},
			}).then(function success(response) {
				deferred.resolve(response.data);
			}, function error(response) {
				deferred.reject(response.data);
			});
			return deferred.promise;
		};

	});
