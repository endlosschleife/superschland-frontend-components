'use strict';

/**
 * @ngdoc service
 * @name superschlandService.GroupService
 * @description
 * # GroupService
 * Service in the superschlandService.
 */
angular.module('superschlandService')
  .service('Superschland.PluginService', function ($http, BACKEND_URL) {

    this.getPlugins = function(callbackFunction) {
        $http.get(BACKEND_URL + '/plugins').then(function(response) {
            callbackFunction(response.data);
        }, function (response) {
            callbackFunction(response.data);
        });
    };

  });
