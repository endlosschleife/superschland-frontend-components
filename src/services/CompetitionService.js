'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.CompetitionService
 * @description
 * # CompetitionService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('Superschland.CompetitionService', ['$http', 'BACKEND_URL', function ($http, BACKEND_URL) {

		this.getCompetitions = function(callbackFunction) {

			$http.get(BACKEND_URL + '/competitions').then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.getCompetition = function(competitionId, callbackFunction) {

			$http.get(BACKEND_URL + '/competition/' + competitionId).then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.updateCompetition = function(competition, callbackFunction) {

			$http.put(BACKEND_URL + '/competition/' + competition.id, competition).then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

		this.addCompetition = function(name, competitionMode, callbackFunction) {

			$http.post(BACKEND_URL + '/competition', {
				"competitionMode": competitionMode,
				"name": name
			}).then(function(response) {
				callbackFunction(response.data);
			}, function (response) {
				callbackFunction(response.data);
			});

		};

	}
	]);
