'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.SuperschlandPredictionGameSettingsService
 * @description
 * # SuperschlandPredictionGameSettingsService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('SuperschlandPredictionGameSettingsService', ['$http', 'BACKEND_URL', '$q', function ($http, BACKEND_URL, $q) {

		this.getSettings = function (shortcut) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/community/' + shortcut + '/prediction-game/settings')
				.then(function (response) {
					deferred.resolve(response.data);
				}, function (response) {
					deferred.reject(response);
				});
			return deferred.promise;
		};

	}
	]);
