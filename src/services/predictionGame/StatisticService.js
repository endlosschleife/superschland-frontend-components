'use strict';

/**
 * @ngdoc service
 * @name yeomanGeneratedProjectApp.SuperschlandPredictionGameStatisticService
 * @description
 * # SuperschlandPredictionGameStatisticService
 * Service in the yeomanGeneratedProjectApp.
 */
angular.module('superschlandService')
	.service('SuperschlandPredictionGameStatisticService', ['$http', 'BACKEND_URL', '$q', function ($http, BACKEND_URL, $q) {

		this.getStatisticRows = function (predictionGameId) {
			var deferred = $q.defer();
			$http.get(BACKEND_URL + '/predictionGame/' + predictionGameId + '/statistic')
				.then(function (response) {
					deferred.resolve(response.data);
				}, function (response) {
					deferred.reject(response);
				});
			return deferred.promise;
		};

	}
]);
